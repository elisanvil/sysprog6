#include <stdio.h>
#include <string.h>
#include "mask.h"

#define SET 1
#define UNSET 0

static int mask_owner, mask_group, mask_other;

int main(){
    int mascara = 0000;
    int *ow = &mask_owner;
    int *gr = &mask_group;
    int *ot = &mask_other;
    int *mask = &mascara;

    char answer[5];

    // 1
    printf("\nPermiso lectura para owner? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(ow,'r', SET); }
    else { setPermiso(ow,'r', UNSET); }

    // 2
    printf("\nPermiso escritura para owner? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(ow,'w', SET); }
    else { setPermiso(ow,'w', UNSET); }

    // 3
    printf("\nPermiso ejecución para owner? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(ow,'x', SET); }
    else { setPermiso(ow,'x', UNSET); }

    // 4 
    printf("\nPermiso lectura para group? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(gr,'r', SET); }
    else { setPermiso(gr,'r', UNSET); }

    // 5 
    printf("\nPermiso escritura para group? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(gr,'w', SET); }
    else { setPermiso(gr,'w', UNSET); }

    // 6 
    printf("\nPermiso ejecución para group? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(gr,'x', SET); }
    else { setPermiso(gr,'x', UNSET); }

    // 7 
    printf("\nPermiso lectura para other? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(ot,'r', SET); }
    else { setPermiso(ot,'r', UNSET); }

    // 8 
    printf("\nPermiso escritura para other? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(ot,'w', SET); }
    else { setPermiso(ot,'w', UNSET); }

    // 9 
    printf("\nPermiso ejecucion para other? [s/n] -> ");
    scanf("%s", answer);        getchar();
    if ( strcmp("s",answer) == 0 ) { setPermiso(ot,'x', SET); }
    else { setPermiso(ot,'x', UNSET); }


    //  Generamos la mascara final
    generateMask(mask, mask_owner, mask_group, mask_other);

    // Mostramos la mascara generada
    printf("\n -> La mascara para los permisos es : %d\n", mascara);

    return 0;
}
