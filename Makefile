#   Archivo makefile para compilacion del programa   #

# -----------------------------------------------------------
# Target que se ejecutará por defecto con el comando make
# -----------------------------------------------------------
all:
	@gcc -o program main.c mask.c -I.
	@echo "    Done. Execute '$ ./program' to continue."

# -----------------------------------------------------------
# Limpiar archivos temporales
# -----------------------------------------------------------
clean:
	@rm -rf *.o program 
	@echo "    Cleaned!"