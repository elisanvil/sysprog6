#include <stdio.h>
#include "mask.h"

/*
	El parámetro mascara es la máscara que 
	se debe mantener para cada nivel de acceso 
	y debe de ser enviado por referencia. 

	El parámetro permiso es un char 
	con los valores "r", "w", o "x" 

	y set es un boolean con valores de  0 o 1. 

	Los valores de 0 y 1 deben de estar definidos 
	con un #define como SET y UNSET. 
*/

void setPermiso(int *mascara, char permiso, int set) {
   
    switch(permiso) {
      	case 'r' :
      		if (set == 1) {
				*mascara = *mascara | 4;
      		}
        	break;
      	case 'w' :
      		if (set == 1) {
				*mascara = *mascara | 2;
      		}
        	break;
      	case 'x' :
      		if (set == 1) {
				*mascara = *mascara | 1;
      		}
        	break;
      	default :
      		*mascara = *mascara;
    }
}


void generateMask(int *mascara, int owner, int group, int other){
	// Sumamos las mascaras
	*mascara = (owner*100) + (group*10) + (other*1);
}
